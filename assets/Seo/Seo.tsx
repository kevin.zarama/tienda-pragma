import React from 'react';
import Head from 'next/head';

const Seo = (props: { title: string, description: string }) => {
  const { title, description } = props;

  return (
    <Head>
      <title>{title}</title>
      <meta name='description' content={description} />
    </Head>
  );
};

export default Seo;
