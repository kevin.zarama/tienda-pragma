import { magento_url } from "assets/constants/api";
import { toast } from "react-toastify";

export async function createCart() {
  const token = sessionStorage.getItem('token');
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ query: `query { customerCart { id } }` }),
    });
    const data = await response.json();
    sessionStorage.setItem('cartToken', data.data.customerCart.id);
  } catch (error) {
    console.error(error);
  };
};

export async function addCartProduct(sku: string) {
  const cartToken = sessionStorage.getItem('cartToken');
  const token = sessionStorage.getItem('token');
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        query: `
        mutation {
          addSimpleProductsToCart(
            input: {
              cart_id: "${cartToken}"
              cart_items: [ { data: { quantity: 1 sku: "${sku}" } } ]
            }
          ) {
            cart { items { uid quantity product { sku stock_status } } }
          }
        }`
      }),
    });
    toast.success("Producto añadido al carrito");
  } catch (error) {
    console.error(error);
  };
};

export async function getProductsCart() {
  const token = sessionStorage.getItem('token');
  const cartToken = sessionStorage.getItem('cartToken')
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        query: `
        query {
          cart(cart_id: "${cartToken}") {
            items {
              uid
              quantity
              product {
                sku
                name
                price_range { maximum_price { regular_price { value } } }
                image { url }
              }
            }
            prices { grand_total { value } }
          }
        }`
      }),
    });
    const data = await response.json();
    return data.data.cart;
  } catch (error) {
    console.error(error);
  };
};

export async function removeItemCartApi(uid: string) {
  const cartToken = sessionStorage.getItem('cartToken')
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        mutation {
          removeItemFromCart(
            input: {
              cart_id: "${cartToken}",
              cart_item_uid: "${uid}"
            }
          )
         {
          cart {
            items { id quantity product { name } }
            prices { grand_total { value currency } }
          }
         }
        }`
      }),
    });
    const data = await response.json();
    toast.success("Producto eliminado del carrito");
  } catch (error) {
    console.error(error);
  };
};

export async function clearCart() {
  const token = sessionStorage.getItem('cartToken')
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        mutation {
          clearCustomerCart(
            cartUid: "8k0Q4MpH2IGahWrTRtqM61YV2MtLPApz"
          ) {
            status
          }
        }`
      }),
    });
    const data = await response.json();
  } catch (error) {
    console.error(error);
  };
};
