import { magento_url } from 'assets/constants/api';

export async function getAllProducts(pageSize: number, currentPage: number) {
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        query: `
        query {
          products(
            pageSize: ${pageSize}
            currentPage: ${currentPage}
            filter: {price: {from: "0" to: "100000000"}}
          ) {
            items {
              sku
              name
              image { url }
              short_description { html }
              price_range { maximum_price { regular_price { value } } }
            }
            page_info {
              total_pages
            }
          }
        }`
      }),
    });
    const data = await response.json();
    return data.data.products;
  } catch (error) {
    console.error(error);
  };
};

export async function getSingleProduct(productId: string) {
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        query: `
        query {
          products(search: "${productId}") {
            items {
              sku
              name
              image { url }
              description { html }
              short_description { html }
              media_gallery { position url }
              price_range { maximum_price { regular_price { value } } }
              related_products {
                sku
                name
                short_description { html }
                price_range { maximum_price { regular_price { value } } }
                image { url }
              }
            }
          }
        }`
      }),
    });
    const data = await response.json();
    return data.data.products.items[0];
  } catch (error) {
    console.error(error);
  };
};

export async function getProductsFilter(uid: String, pageSize: number, currentPage: number) {
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        query: `
        query {
          products(
            pageSize: ${pageSize}
            currentPage: ${currentPage}
            filter: { category_uid: { in: ["${uid}"] } }
          ) {
            items {
              sku
              name
              image { url }
              description { html }
              short_description { html }
              media_gallery { position url }
              price_range { maximum_price { regular_price { value } } }
            }
            page_info {
              total_pages
            }
          }
        }`
      }),
    });
    const data = await response.json();
    return data.data.products;
  } catch (error) {
    console.error(error);
  };
};

export async function getComputerCategoryList() {
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        query: `
        query {
          categoryList(filters: { category_uid: { eq: "Mw==" } }) {
            children { uid name }
          }
        }`
      }),
    });
    const data = await response.json();
    return data.data.categoryList[0];
  } catch (error) {
    console.error(error);
  };
};
