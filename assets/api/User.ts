import { magento_url } from 'assets/constants/api';

export async function loginUser(email: string, password: string) {
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        mutation {
          generateCustomerToken(email: "${email}", password: "${password}") {
            token
          }
        }`
      }),
    });
    const data = await response.json();
    return data.data.generateCustomerToken;
  } catch (error) {
    console.error(error);
  };
};

export async function registerUser(firstname: string, email: string, password: string) {
  try {
    const response = await fetch(magento_url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        mutation {
          createCustomer(
            input: {
              firstname: "${firstname}"
              lastname: "${firstname}"
              email: "${email}"
              password: "${password}"
              is_subscribed: true
            }
          ) {
            customer { firstname lastname email is_subscribed }
          }
        }`
      }),
    });
    const data = await response.json();
    return data.data.createCustomer.customer;
  } catch (error) {
    console.error(error);
  };
};
