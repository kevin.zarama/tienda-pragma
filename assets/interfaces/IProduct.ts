export interface ProductInterface {
  name: string;
  sku: string;
  price_range: {
    maximum_price: {
      regular_price: {
        value: number;
      };
    };
  };
  short_description: {
    html: string;
  };
  description: {
    html: string;
  };
  media_gallery: [
    {
      position: string;
      url: string;
    }
  ];
  image: {
    url: string;
  };
  related_products: ProductInterface[];
};
