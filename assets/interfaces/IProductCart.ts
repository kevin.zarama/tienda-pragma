import { ProductCartElementInterface } from "./IProductCartElement";

export interface ProductCartInterface {
  items: ProductCartElementInterface[],
  prices: {
    grand_total: {
      value: number,
    },
  },
};
