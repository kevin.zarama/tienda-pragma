export interface ProductCartElementInterface {
  uid: string,
  product: {
    name: string,
    sku: string,
    image: {
      url: string,
    },
    price_range: {
      maximum_price: {
        regular_price: {
          value: number,
        }
      }
    }
  },
  quantity: number,
};
