import React from 'react';

const ButtonPragma = (props: { text: string, clickFunction: Function }) => {
  const { text, clickFunction } = props;

  return (
    <button className="pragma__btn" onClick={() => clickFunction()}>{text}</button>
  );
};

export default ButtonPragma;
