import React, { ChangeEventHandler } from 'react';

const CheckoutLabel = (
  props: {
    name: string,
    text: string,
    required: string,
    handleChange: ChangeEventHandler<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>
  }) => {
  const { name, text, required, handleChange } = props;

  return (
    <label className="userData__label">{text}:<span>{required}</span>
      <input className='userData__input' name={name} type="text" onChange={handleChange} />
    </label>
  );
};

export default CheckoutLabel;
