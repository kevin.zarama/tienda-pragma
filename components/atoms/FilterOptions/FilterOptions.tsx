import { map } from 'lodash';
import React from 'react';

const FilterOptions = (props: { title: string, filters: { uid: string, name: string }[], handleCheckBrand: Function }) => {
  const { title, filters, handleCheckBrand } = props;

  return (
    <div className='brand_filter checkbox__container'>
      <span>{title}</span>
      <div className='brand_checkbox checkbox'>
        {map(filters, (filter) => {
          return (
            <div key={filter['uid']}>
              <label><input type='checkbox' onClick={() => handleCheckBrand(filter['uid'])} />{filter['name']}</label><br />
            </div>
          )
        })}
      </div>
    </div>
  );
};

export default FilterOptions;
