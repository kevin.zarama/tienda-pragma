import React from 'react';
import { Pagination } from 'semantic-ui-react';

const PaginationElement = (props: { currentPage: number, setCurrentPage: Function, totalPages: number }) => {
  const { currentPage, setCurrentPage, totalPages } = props;

  const handlePaginationChange = (event: any, value: any) => {
    setCurrentPage(value.activePage);
  }

  return (
    <Pagination count={currentPage} totalPages={totalPages} onPageChange={handlePaginationChange} />
  );
};

export default PaginationElement;
