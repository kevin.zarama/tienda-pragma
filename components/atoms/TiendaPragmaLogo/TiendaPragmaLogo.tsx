/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Link from 'next/link';

const TiendaPragmaLogo = () => {
  return (
    <Link href='/' passHref={true}>
      <img
        className='logo__header'
        src='https://game-store-next.s3.sa-east-1.amazonaws.com/logo.png'
        alt='Logo Tienda Pragma'
      />
    </Link>
  );
};

export default TiendaPragmaLogo;
