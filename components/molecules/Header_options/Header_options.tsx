import Link from 'next/link';
import Image from 'next/image';
import React, { useEffect } from 'react';
import { Icon } from 'semantic-ui-react';
import { loginUser, registerUser } from 'assets/api/User';
import { signIn, signOut, useSession } from 'next-auth/react';

const Header_options = () => {
  const { data: session } = useSession();

  useEffect(() => {
    (async () => {
      if (session) {
        const response = await loginUser(session.user?.email!, 'TestPassword12');
        if (!response) {
          registerUser(session.user?.name!, session.user?.email!, 'TestPassword12')
            .then(async () => {
              const token = await loginUser(session.user?.email!, 'TestPassword12');
              sessionStorage.setItem('token', token.token)
            });
        } else {
          sessionStorage.setItem('token', response.token)
        };
      };
    })();
  }, [session]);

  return (
    <>
      <div className='header__options__container_web'>
        <Link href='/'>Inicio</Link>
        <Link href='/catalog'>Catálogo</Link>
        {session ?
          <>
            <Link href='/cart' passHref={true}><span><Icon name='cart' /></span></Link>
            <Link href='/user' passHref={true}><span><Image width={35} height={35} src={session.user?.image!} alt='imagen usuario' />{session.user?.name}</span></Link>
            <Icon name='sign-out' onClick={signOut} />
          </>
          :
          <Icon name='sign-in' onClick={signIn} />
        }
      </div>
      <div className='header__options__container_mobile'>
        <Link href='/'>Inicio</Link>
        <Link href='/catalog'>Catálogo</Link>
        {session ?
          <>
            <Link href='/cart' passHref={true}><span><Icon name='cart' />Carrito</span></Link>
            <Link href='/user' passHref={true}><span><Image width={35} height={35} src={session.user?.image!} alt='imagen usuario' />{session.user?.name}</span></Link>
            <span><Icon name='sign-out' onClick={signOut} />Sign Out</span>
          </>
          :
          <span><Icon name='sign-in' onClick={signIn} />Sign In</span>
        }
      </div>
    </>
  );
};

export default Header_options;
