/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Link from 'next/link';
import { signIn, useSession } from 'next-auth/react';
import { ProductInterface } from 'assets/interfaces/IProduct';

const ProductCard = (props: { product: ProductInterface }) => {
  const { product } = props;
  const { data: session } = useSession();

  return (
    <Link href={`/product/${product.sku}`} passHref >
      <div className='productCard__container'>
        <img className='product__image' src={product.image.url} alt={product.name} />
        <h2>{product.name}</h2>
        {session ?
          <h2>{product.price_range.maximum_price.regular_price.value}</h2>
          :
          <h2 className='loginIn_prices' onClick={() => signIn()}>Inicia sesion para ver precios</h2>
        }
        <span>{product.short_description.html}</span>
      </div>
    </Link>
  );
};

export default ProductCard;
