/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { Icon } from 'semantic-ui-react';
import { ProductCartElementInterface } from 'assets/interfaces/IProductCartElement';

const CartProduct = (props: { productCart: ProductCartElementInterface, removeItemCart: Function }) => {
  const { productCart, removeItemCart } = props;

  return (
    <>
      <div className='cartProduct__container__mobile'>
        <Icon className='trash__icon' name='trash' onClick={() => removeItemCart(productCart.uid)} />
        <img className='product_cart__image' src={productCart.product.image.url} alt={productCart.product.name} />
        <div className='description__container'>
          <span>Producto: {productCart.product.name}</span><br />
          <span>Precio: {productCart.product.price_range.maximum_price.regular_price.value}</span><br />
          <span>Cantidad: {productCart.quantity}</span><br />
          <span>Subtotal: {productCart.quantity * productCart.product.price_range.maximum_price.regular_price.value}</span>
        </div>
      </div>
      <div className="cartProduct__container__web">
        <Icon className='trash__icon' name='trash' onClick={() => removeItemCart(productCart.uid)} />
        <div className="product_info">
          <h2>{productCart.product.name}</h2>
          <img className="product__image" src={productCart.product.image.url} alt={productCart.product.name} />
        </div>
        <span>{productCart.product.price_range.maximum_price.regular_price.value}</span>
        <span>{productCart.quantity}</span>
        <span>{productCart.quantity * productCart.product.price_range.maximum_price.regular_price.value}</span>
      </div>
    </>
  );
};

export default CartProduct;
