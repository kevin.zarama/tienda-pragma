import React from 'react';

import ButtonPragma from '@atoms/ButtonPragma/ButtonPragma';

const CartTotal = (props: { subtotal: number, buttonText: string, buttonFunction: Function }) => {
  const { subtotal, buttonText, buttonFunction } = props;
  const totalEnvio = 15000;

  return (
    <>
      <span>Subtotal: {subtotal}</span>
      <span>Envio: Precio fijo: {totalEnvio}</span>
      <span>Total: {subtotal + totalEnvio}</span>
      <ButtonPragma text={buttonText} clickFunction={buttonFunction} />
    </>
  );
};

export default CartTotal;
