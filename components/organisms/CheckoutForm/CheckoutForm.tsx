import CheckoutLabel from '@atoms/CheckoutLabel/CheckoutLabel';
import React, { ChangeEventHandler, FormEventHandler } from 'react';

const CheckoutForm = (
  props: {
    secondaryAdress: boolean,
    openSecondaryAddress: ChangeEventHandler<HTMLInputElement> | null,
    handleChange: ChangeEventHandler<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>
  }) => {

  const { secondaryAdress, openSecondaryAddress, handleChange } = props;

  return (
    <form>
      <CheckoutLabel name="name" text="Nombre" required="*" handleChange={handleChange} />
      <CheckoutLabel name="lastname" text="Apellido" required="*" handleChange={handleChange} />
      <CheckoutLabel name="identification" text="Número de identificación (optional):" required="" handleChange={handleChange} />
      <CheckoutLabel name="address" text="Direccion de la calle:" required="*" handleChange={handleChange} />
      <CheckoutLabel name="addressDescription" text="Apartamento, habitación, escalera, etc. (optional):" required="" handleChange={handleChange} />
      <CheckoutLabel name="city" text="Ciudad:" required="*" handleChange={handleChange} />
      <CheckoutLabel name="state" text="Departamento:" required="*" handleChange={handleChange} />
      <CheckoutLabel name="phone" text="Teléfono:" required="*" handleChange={handleChange} />

      {openSecondaryAddress ?
        <>
          <label className="userData__label">Descuento de nómina:<span>*</span>
            <select className='userData__select' name="discount" onChange={handleChange} >
              <option></option>
              <option>(1) Una cuota</option>
              <option>(2) Dos cuota</option>
              <option>(3) Tres cuota</option>
            </select>
          </label>
          <label className="userData__label"><input className="userData__checkbox" type="checkbox" name="otherAddress" onChange={openSecondaryAddress} />
            ¿Enviar a una dirección diferente?
          </label><br />
        </>
        :
        <></>
      }

      {!secondaryAdress ?
        <label className="userData__label">Notas del pedido:
          <textarea className='userData__textarea' name="notes" placeholder='Notas sobre tu perido, ejemplo: notas especiales para la entrega.' onChange={handleChange} />
        </label>
        :
        <></>
      }
    </form>
  );
};

export default CheckoutForm;
