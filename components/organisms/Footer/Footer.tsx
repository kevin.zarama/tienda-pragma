import React from 'react';
import Link from 'next/link';

import TiendaPragmaLogo from '@atoms/TiendaPragmaLogo/TiendaPragmaLogo';

const Footer = () => {
  return (
    <div className='footer__container'>
      <TiendaPragmaLogo />

      <div className='footer__links'>
        <Link href='/'>Inicio</Link>
        <Link href='/'>Información</Link>
        <Link href='/catalog'>Catálogo</Link>
        <Link href='/'>Contacto</Link>
      </div>

      <div className='footer__images'>
        <Link passHref href='https://facebook.com/'><img src='https://game-store-next.s3.sa-east-1.amazonaws.com/facebook.png' alt='Facebook logo' /></Link>
        <Link passHref href='https://twitter.com/'><img src='https://game-store-next.s3.sa-east-1.amazonaws.com/twitter.webp' alt='Twitter logo' /></Link>
        <Link passHref href='http://youtube.com/'><img src='https://game-store-next.s3.sa-east-1.amazonaws.com/youtube.webp' alt='Youtube logo' /></Link>
      </div>
    </div>
  );
};

export default Footer;
