import React, { useState } from 'react';
import { FiMenu } from 'react-icons/fi';
import { MdClose } from 'react-icons/md';

import Header_options from '@molecules/Header_options/Header_options';
import TiendaPragmaLogo from '@atoms/TiendaPragmaLogo/TiendaPragmaLogo';

const Header = () => {
  const [navbarOpen, setNavbarOpen] = useState(false);

  const handleToggle = () => {
    setNavbarOpen(!navbarOpen);
  };

  const closeMenu = () => {
    setNavbarOpen(false);
  };

  return (
    <>
      <nav className='mobile_header__container'>
        <div className='mobile__navbar'>
          <button onClick={handleToggle}>
            {navbarOpen ? (
              <MdClose style={{ color: '#fff', width: '40px', height: '40px' }} />
            ) : (
              <FiMenu
                style={{ color: '#7b7b7b', width: '40px', height: '40px' }}
              />
            )}
          </button>
          <TiendaPragmaLogo />
        </div>
        <div
          className={`menuNav${navbarOpen ? ' showMenu' : ''}`}
          onClick={() => closeMenu()}
        >
          <Header_options />
        </div>
      </nav>

      <header className='web_navbar'>
        <TiendaPragmaLogo />
        <Header_options />
      </header>
    </>
  );
};

export default Header;
