import { map } from 'lodash';
import Router from 'next/router';
import React, { useEffect, useState } from 'react';
import { getProductsCart, removeItemCartApi } from 'assets/api/Cart';
import { ProductCartInterface } from 'assets/interfaces/IProductCart';

import Layout from '@templates/Layout/Layout';
import CartTotal from '@organisms/CartTotal/CartTotal';
import CartProduct from '@organisms/CartProduct/CartProduct';

const Cart = () => {
  const [products, setProducts] = useState<ProductCartInterface>();

  useEffect(() => {
    (async () => {
      const response = await getProductsCart();
      setProducts(response);
    })();
  }, []);

  const removeItemCart = async (uid: string) => {
    await removeItemCartApi(uid);
    const response = await getProductsCart();
    setProducts(response);
  }

  return (
    <Layout title="Carrito" description="Carrito Tienda Pragma">
      <div className="cart__container">
        <h1>Carrito</h1>
        {products && products.items.length > 0 ?
          <>
            <div className="cart_headers">
              <h2></h2>
              <h2>Producto</h2>
              <h2>Precio</h2>
              <h2>Cantidad</h2>
              <h2>Subtotal</h2>
            </div>
            {map(products?.items, (product) => {
              return (
                <CartProduct key={product.uid} productCart={product} removeItemCart={removeItemCart} />
              );
            })}
            <div className='cartTotal__container'>
              <h2>Total del carrito</h2>
              <CartTotal buttonText={'Finalizar compra'} subtotal={products?.prices.grand_total.value} buttonFunction={() => { Router.push('/checkout') }} />
            </div>
          </>
          :
          <h2>No hay productos en tu carrito.</h2>
        }
      </div>
    </Layout>
  );
};

export default Cart;
