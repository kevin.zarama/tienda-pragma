import { map } from 'lodash';
import React, { useEffect, useState } from 'react';
import { ProductInterface } from 'assets/interfaces/IProduct';
import { getAllProducts, getComputerCategoryList, getProductsFilter } from 'assets/api/Product';

import Layout from '@templates/Layout/Layout';
import ProductCard from '@molecules/ProductCard/ProductCard';
import FilterOptions from '@atoms/FilterOptions/FilterOptions';
import PaginationElement from '@atoms/PaginationElement/PaginationElement';

const pageSize = 4;

const Catalog = () => {
  const [brands, setBrands] = useState<{ uid: string, name: string }[]>([]);
  const [checkBrand, setCheckBrand] = useState<String[]>([]);
  const [products, setProducts] = useState<ProductInterface[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    (async () => {
      const response = await getAllProducts(pageSize, currentPage);
      setProducts(response.items);
      setTotalPages(response.page_info.total_pages);
    })();
  }, [currentPage]);

  useEffect(() => {
    (async () => {
      const response = await getComputerCategoryList();
      setBrands(response.children);
    })();
  }, []);

  const handleCheckBrand = async (checkedBrand: string) => {
    const checkedBrandUpdate = checkBrand;
    const index = checkedBrandUpdate.indexOf(checkedBrand);
    if (index >= 0) {
      checkedBrandUpdate.splice(index, 1);
    } else {
      checkedBrandUpdate.push(checkedBrand);
    }
    setCheckBrand(checkedBrandUpdate);
    if (checkBrand.length === 0) {
      const response = await getAllProducts(pageSize, currentPage);
      setProducts(response.items);
      setTotalPages(response.page_info.total_pages);
    } else {
      const response = await getProductsFilter(checkBrand.toString().replaceAll(',', '" "'), pageSize, currentPage);
      setProducts(response.items);
      setTotalPages(response.page_info.total_pages);
    };
  };

  const handleCheckColor = async (checkedColor: string) => {
    console.log(checkedColor);
  };

  return (
    <Layout title='Catálogo Tienda Pragma' description='Tienda Pragma'>
      <div className='catalogPage__container'>
        <h1>Catálogo de Productos</h1>
        <div className='catalog__container'>
          <div className='filters__container'>
            <FilterOptions title={'Filtrar por marca'} filters={brands} handleCheckBrand={handleCheckBrand} />
            <FilterOptions title={'Filtrar por color'} filters={[{ uid: '1', name: 'White' }, { uid: '2', name: 'Black' }]} handleCheckBrand={handleCheckColor} />
          </div>
          <div className='catalog_products__container'>
            {map(products, (product => {
              return (
                <ProductCard key={product.sku} product={product} />
              )
            }))}
          </div>
        </div>
        <PaginationElement currentPage={currentPage} setCurrentPage={setCurrentPage} totalPages={totalPages} />
      </div>
    </Layout>
  );
};

export default Catalog;
