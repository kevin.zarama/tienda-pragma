import React, { useState } from 'react';

import Layout from '@templates/Layout/Layout';
import CartTotal from '@organisms/CartTotal/CartTotal';
import CheckoutForm from '@organisms/CheckoutForm/CheckoutForm';

const Checkout = () => {
  const [secondaryAddress, setSecondaryAddress] = useState(false);

  const [state, setState] = useState({
    name: '',
    lastname: '',
    identification: '',
    address: '',
    addressDescription: '',
    city: '',
    state: '',
    phone: '',
    discount: '',
    otherAddress: false,
    notes: ''
  });

  const [secondaryState, setSecondaryState] = useState({
    name: '',
    lastname: '',
    identification: '',
    address: '',
    addressDescription: '',
    city: '',
    state: '',
    phone: '',
    notes: ''
  });

  const handleChange = (event: any) => {
    const value = event.target.value;
    const inputName = event.target.name;
    let newState = state;
    newState = {
      ...state,
      [inputName]: value,
    }
    setState(newState);
  };

  const openSecondaryAddress = (event: any) => {
    setSecondaryAddress(!secondaryAddress);
  }

  const handleSecondaryChange = (event: any) => {
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
    const inputName = event.target.name;
    let newState = secondaryState;
    newState = {
      ...secondaryState,
      [inputName]: value,
    }
    setSecondaryState(newState);
  };

  const sendOrder = () => {
    if (!secondaryAddress) {
      if (
        state.name == '' ||
        state.lastname == '' ||
        state.address == '' ||
        state.city == '' ||
        state.state == '' ||
        state.phone == '' ||
        state.discount == ''
      ) {
        alert('Los campos con * son requeridos');
      } else {
        console.log(state);
      }
    } else {
      if (
        state.name == '' ||
        state.lastname == '' ||
        state.address == '' ||
        state.city == '' ||
        state.state == '' ||
        state.phone == '' ||
        state.discount == '' ||
        secondaryState.name == '' ||
        secondaryState.lastname == '' ||
        secondaryState.address == '' ||
        secondaryState.city == '' ||
        secondaryState.state == '' ||
        secondaryState.phone == ''
      ) {
        console.log({ ...state, ...secondaryState });
      }
    }
  }

  return (
    <Layout title="Checkout" description="Checkout Tienda Pragma" >
      <div className="checkout__container">
        <div className="checkout__details">
          <h1>Detalles de facturación</h1>
          <CheckoutForm
            secondaryAdress={secondaryAddress}
            handleChange={handleChange}
            openSecondaryAddress={openSecondaryAddress}
          />
          {secondaryAddress ?
            <CheckoutForm
              secondaryAdress={!secondaryAddress}
              handleChange={handleSecondaryChange}
              openSecondaryAddress={null}
            />
            :
            <></>
          }
        </div>
        <div className='resume_order__container'>
          <h2>Pedido</h2>
          <CartTotal buttonText={'Realizar compra'} subtotal={1000000000} buttonFunction={sendOrder} />
        </div>
      </div>
    </Layout>
  );
};

export default Checkout;
