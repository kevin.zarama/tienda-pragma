/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react';
import { map } from 'lodash';
import { Carousel } from 'react-responsive-carousel';

import Layout from '@templates/Layout/Layout';
import { getAllProducts } from 'assets/api/Product';
import ProductCard from '@molecules/ProductCard/ProductCard';
import { ProductInterface } from 'assets/interfaces/IProduct';
import PaginationElement from '@atoms/PaginationElement/PaginationElement';

const banners = [
  {
    id: '1',
    image: 'https://game-store-next.s3.sa-east-1.amazonaws.com/1.jpg',
    alt: 'Banner 1'
  },
  {
    id: '2',
    image: 'https://game-store-next.s3.sa-east-1.amazonaws.com/2.jpg',
    alt: 'Banner 2'
  },
  {
    id: '3',
    image: 'https://game-store-next.s3.sa-east-1.amazonaws.com/3.webp',
    alt: 'Banner 3'
  },
  {
    id: '4',
    image: 'https://game-store-next.s3.sa-east-1.amazonaws.com/4.jfif',
    alt: 'Banner 4'
  },
  {
    id: '5',
    image: 'https://game-store-next.s3.sa-east-1.amazonaws.com/5.jpg',
    alt: 'Banner 5'
  }
];

const pageSize = 6;

const Index = () => {
  const [products, setProducts] = useState<ProductInterface[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    (async () => {
      const response = await getAllProducts(pageSize, currentPage);
      setProducts(response.items);
      setTotalPages(response.page_info.total_pages);
    })();
  }, [currentPage]);

  return (
    <Layout title='Tienda Pragma' description='Tienda Pragma'>
      <Carousel className='banner__container' showThumbs={false} emulateTouch infiniteLoop centerMode centerSlidePercentage={70}>
        {map(banners, (banner => {
          return (
            <img key={banner.id} src={banner.image} alt={banner.alt} className='banner__images' />
          )
        }))}
      </Carousel>
      <div className='fearutedProducts__container'>
        <h1>Destacados/Ofertas</h1>
        <div className='products__container'>
          {map(products, (product => {
            return (
              <ProductCard key={product.sku} product={product} />
            )
          }))}
        </div>
        <PaginationElement currentPage={currentPage} setCurrentPage={setCurrentPage} totalPages={totalPages} />
      </div>
    </Layout>
  );
};

export default Index;
