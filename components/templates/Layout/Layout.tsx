import React from 'react';

import Seo from 'assets/Seo/Seo';
import Footer from '@organisms/Footer/Footer';
import Header from '@organisms/Header/Header';

const Layout = (props: { children: React.ReactNode, title: string, description: string }) => {
  const { children, title, description } = props;

  return (
    <>
      <Seo title={title} description={description} />
      <Header />
      {children}
      <Footer />
    </>
  );
};

export default Layout;
