/* eslint-disable @next/next/no-img-element */
import { map } from 'lodash';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { Carousel } from 'react-responsive-carousel';
import { signIn, useSession } from 'next-auth/react';
import { getSingleProduct } from 'assets/api/Product';
import { ProductInterface } from 'assets/interfaces/IProduct';

import Layout from '@templates/Layout/Layout';
import ProductCard from '@molecules/ProductCard/ProductCard';
import ButtonPragma from '@atoms/ButtonPragma/ButtonPragma';
import { addCartProduct, createCart } from 'assets/api/Cart';

const Product = () => {
  const { query } = useRouter();
  const [product, setProduct] = useState<ProductInterface>();
  const { data: session } = useSession();

  useEffect(() => {
    (async () => {
      const response = await getSingleProduct(query.id as string);
      setProduct(response);
    })();
  }, [query]);

  const addToCart = () => {
    if (!sessionStorage.getItem("cartToken")) {
      createCart();
    };
    addCartProduct(product!.sku);
  };

  return (
    <Layout title='Tienda Pragma' description='Tienda Pragma'>
      <div className='nav_container'>
        <Link href='/'>Inicio</Link>/<Link href='/catalog'>Catálogo</Link>/{product?.name}
      </div>
      <div className='product_page__container'>
        <div className='product_description__container'>
          <Carousel className='images__slider' infiniteLoop emulateTouch>
            {map(product?.media_gallery, (image) => {
              return (
                <img key={image.position} src={image.url} alt={product?.name} />
              );
            })}
          </Carousel>
          <div>
            <h1>{product?.name}</h1>
            <h2>Precio:</h2>
            {session ?
              <h2>{product?.price_range.maximum_price.regular_price.value}</h2>
              :
              <h2 className='loginIn_prices' onClick={() => signIn()}>Inicia sesion para ver precios</h2>
            }
            <h2>Especificaciones:</h2>
            {/* {map(product.specs, (spec) => {
              return (
                <p key={spec}>- {spec}</p>
              );
            })} */}
            {session ?
              <ButtonPragma text="Añadir al carrito" clickFunction={addToCart} />
              :
              <></>
            }
          </div>
        </div>
        <h2>Descripción</h2>
        <span>{product?.short_description.html}</span>
        {product?.related_products && product?.related_products.length > 0 ?
          <div className='related_products__container'>
            <h2>Productos relacionados</h2>
            <div className='related_products'>
              {map(product?.related_products, (product) => {
                return (
                  <ProductCard key={product.sku} product={product} />
                );
              })}
            </div>
          </div>
          :
          <></>
        }
      </div>
    </Layout>
  );
};

export default Product;
