/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
}

module.exports = {
  nextConfig,
  images: {
    domains: ['game-store-next.s3.sa-east-1.amazonaws.com', 'lh3.googleusercontent.com'],
    formats: ['image/avif', 'image/webp'],
  },
}
