import type { AppProps } from 'next/app';
import 'semantic-ui-css/semantic.min.css';
import { SessionProvider } from 'next-auth/react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import "react-toastify/dist/ReactToastify.css";
import '../styles/index.scss';
import { ToastContainer } from 'react-toastify';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <SessionProvider session={pageProps.session}>
      <Component {...pageProps} />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover
      />
    </SessionProvider>
  );
};

export default MyApp;
