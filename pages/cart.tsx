import React from 'react';

import Cart from '@templates/Cart/Cart';

const cart = () => {
  return (
    <Cart />
  );
};

export default cart;
