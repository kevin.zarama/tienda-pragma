import React from 'react';
import Catalog from '@templates/Catalog/Catalog';

const catalog = () => {
  return (
    <Catalog />
  );
};

export default catalog;
