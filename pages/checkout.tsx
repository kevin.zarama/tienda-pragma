import Checkout from '@templates/Checkout/Checkout';
import React from 'react';

const checkout = () => {
  return (
    <Checkout />
  );
};

export default checkout;
