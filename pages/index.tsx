import type { NextPage } from 'next';
import Index from '@templates/Index/Index';

const Home: NextPage = () => {
  return (
    <Index />
  );
};

export default Home;
