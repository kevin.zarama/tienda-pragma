import React from 'react';

import Product_template from '../../components/templates/Product_template/Product_template';

const Product = () => {
  return (
    <Product_template />
  );
};

export default Product;
